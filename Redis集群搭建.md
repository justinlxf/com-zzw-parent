### Redis3.2.10 Cluster集群搭建  

服务器环境：  
10.25.83.225  
10.25.13.236  
每台服务器搭建2个节点，组成4个主节点的redis集群。  

#### redis cluster安装  

##### 下载和编译安装  
cd ~  
wget http://download.redis.io/releases/redis-3.2.10.tar.gz  
tar -zxvf redis-3.2.10.tar.gz  
cd redis-3.2.10  
make  
make install  

##### 创建redis节点  

首先分别在10.25.83.225和10.25.13.236上创建2个节点:  
mkdir -p /usr/local/redis_cluster  
cd /usr/local/redis_cluster  
mkdir 6380 6381  
cp /root/redis-3.2.10/redis.conf 6380  
cp /root/redis-3.2.10/redis.conf 6381  
其次分别对6380、6381文件夹中的2个redis.conf文件修改对应的配置： 
~~~~
bind 10.0.0.245 127.0.0.1
port 6381
tcp-backlog 511
timeout 0
tcp-keepalive 300
daemonize yes
supervised no
pidfile "/usr/local/redis_cluster/redis6381.pid"
loglevel notice
logfile "/usr/local/redis_cluster/redis6381.log"
syslog-enabled no
syslog-ident "redis"
databases 16
save 900 1
save 300 10
save 60 10000
stop-writes-on-bgsave-error yes
rdbcompression yes
rdbchecksum yes
dbfilename "dump6381.rdb"
dir "/usr/local/redis_cluster"
slave-serve-stale-data yes
slave-read-only yes
repl-diskless-sync no
repl-diskless-sync-delay 5
repl-disable-tcp-nodelay no
slave-priority 100
maxclients 10000
maxmemory 7812500kb
maxmemory-policy volatile-lru
appendonly yes
appendfilename "appendonly6381.aof"
appendfsync everysec
no-appendfsync-on-rewrite no
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated yes
lua-time-limit 5000
cluster-enabled yes
cluster-config-file "nodes-6381.conf"
cluster-node-timeout 5000
slowlog-log-slower-than 10000
slowlog-max-len 128
latency-monitor-threshold 0
notify-keyspace-events ""
hash-max-ziplist-entries 512
hash-max-ziplist-value 64
list-max-ziplist-size -2
list-compress-depth 0
set-max-intset-entries 512
zset-max-ziplist-entries 128
zset-max-ziplist-value 64
hll-sparse-max-bytes 3000
activerehashing yes
client-output-buffer-limit normal 0 0 0
client-output-buffer-limit slave 256mb 64mb 60
client-output-buffer-limit pubsub 32mb 8mb 60
hz 10
aof-rewrite-incremental-fsync yes 
~~~~

##### 启动节点  

redis-server /usr/local/redis_cluster/6380/redis.conf &  
redis-server /usr/local/redis_cluster/6381/redis.conf &  

#### 创建集群
安装ruby  
yum -y install ruby ruby-devel rubygems rpm-build  
gem install redis  
如若报错  
ERROR:  Error installing redis:  
    redis requires Ruby version >= 2.2.2.  
解决方法采用rvm来更新ruby  
gpg2 --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3  
curl -L get.rvm.io | bash -s stable  
source /usr/local/rvm/scripts/rvm  
rvm install 2.3.4  
rvm use 2.3.4  
rvm use 2.3.4 --default  
rvm remove 2.0.0  
ruby --version  

确认所有的节点都启动，使用redis-trib.rb命令参数create创建集群  
/root/redis-3.2.10/src/redis-trib.rb create --replicas 0 10.25.83.225:6380 10.25.83.225:6381 10.25.13.236:6380 10.25.13.236:6381  
根据提示，确认集群信息无误后输入yes回车即可  
在任意一个节点查看集群节点信息: 
/root/redis-3.2.10/src/redis-trib.rb check 10.25.83.225:6380  

#### 服务化
编写脚本/etc/init.d/redis  
~~~~
#!/bin/bash
# chkconfig:    2345 90 10
# description:  Simple Redis init.d script conceived to work on linux systems
#

script(){
    REDISPORT=$2
    REDIDIP=`/sbin/ifconfig -a|grep inet|grep 10.25|awk '{print $2}'|tr -d "addr:"`
    EXEC=/usr/local/bin/redis-server
    CLIEXEC=/usr/local/bin/redis-cli

    PIDFILE=/usr/local/redis_cluster/redis${REDISPORT}.pid
    CONF="/usr/local/redis_cluster/${REDISPORT}/redis.conf"

    case "$1" in
        start)
            if [ -f $PIDFILE ];then
                echo "$PIDFILE exists,process is already running or crashed"
            else
                echo "Starting Redis server..."
                $EXEC $CONF
            fi
        ;;
        stop)
            if [ ! -f $PIDFILE ];then
                echo "$PIDFILE does not exists,process is not running"
            else
                PID=$(cat $PIDFILE)
                echo "Stopping..."
                $CLIEXEC -h $REDIDIP -p $REDISPORT shutdown
                while [ -x /proc/${PID} ];do
                    echo "Waiting for Redis to shutdown..."
                    sleep 1
                done
                echo "Redis stopped"
            fi
        ;;
        restart|force-reload)   
            ${0} stop
            ${0} start
        ;;
        *)
            echo "Please use start or stop as first argument"
        ;;
    esac
}

script $1 6380
script $1 6381
~~~~

增加脚本的可执行权限  
chmod +x /etc/init.d/redis  
添加脚本到开机自动启动项目中  
cd /etc/rc.d/init.d  
chkconfig --add redis  
chkconfig redis on  
